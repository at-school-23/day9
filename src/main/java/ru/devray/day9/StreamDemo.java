package ru.devray.day9;

import ru.devray.day9.dto.Box;
import ru.devray.day9.dto.Person;
import ru.devray.day9.dto.Student;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamDemo {

    public static void main(String[] args) {
        Stream.of(23, 4, 5, -9, 10)
                .sorted()
                .filter(i -> i > 5)
                .limit(2)
                .map(i -> "number " + i);

        //
        List<Student> students = new ArrayList<>();
        students.add(new Student("Говард", "Рорк", 25, 4));
        students.add(new Student("Гарри", "Поттер", 17, 2));
        students.add(new Student("Хари", "Селдон", 43, 5));
        students.add(new Student("Константин", "Циолковский", 34, 2));
        students.add(new Student("Эраст", "Фандорин", 34, 2));
        students.add(new Student("Илон", "Маск", 28, 1));
        students.add(new Student("Дэвид", "Мартинез", 28, 1));
        students.add(new Student("Джон", "Шепард", 31, 2));

        //отсортируем по алфавиту
        //заберем 5 студентов
        //преобразовать студентов к типу Person
        //List<Person>
        List<Person> collect = students.stream()
                .sorted((o1, o2) -> o1.firstName.compareTo(o2.firstName))
                .limit(5)
                .map(s -> new Person(s.firstName + " " + s.lastName, s.age))
                .collect(Collectors.toList());

        //Method reference

        System.out.println(collect);


        students.stream()
                .sorted((o1, o2) -> o1.firstName.compareTo(o2.firstName))
                .limit(5)
                .map(s -> new Person(s.firstName + " " + s.lastName, s.age))
                .forEach(System.out::println);

        Stream.of(12, 34, 62, 3)
                .map(Box::new)
                .forEach(System.out::println);
    }

}
