package ru.devray.day9;

import ru.devray.day9.dto.Car;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StreamsDemo2 {
    public static void main(String[] args) {
        List<Car> cars = new ArrayList<>();
        cars.add(new Car("Kia", "Rio", 1300000));
        cars.add(new Car("Hyundai", "Sonata", 2650000));
        cars.add(new Car("Volkswagen", "Polo", 1400000));
        cars.add(new Car("Lada", "Vesta SW", 2100000));
        cars.add(new Car("Haval", "Jolion", 2500000));

        List<Car> twoCheapestAuto = getTwoCheapestAuto(cars);
        System.out.println(twoCheapestAuto);

    }

    //два самых дешевых авто
    public static List<Car> getTwoCheapestAuto(List<Car> cars1) {
        List<Car> cheapestCars = cars1.stream()
                .sorted((c1, c2) -> c1.price - c2.price)
                .limit(2)
                .collect(Collectors.toList());
        return cheapestCars;
    }
}
