package ru.devray.day9.dto;


//DTO
public class Student {

    public static int studentCount = 0;

    public int id;
    public String firstName;
    public String lastName;
    public int age;
    public int yearOfStudy;

    public Student(String firstName, String lastName, int age, int yearOfStudy) {
        this.id = ++studentCount;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.yearOfStudy = yearOfStudy;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", yearOfStudy=" + yearOfStudy +
                '}';
    }
}
