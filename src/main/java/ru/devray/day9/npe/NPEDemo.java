package ru.devray.day9.npe;

public class NPEDemo {
    public static void main(String[] args) {
        String carModel = CarDataBase.getCarByRegisterNumber2("у961мк54").orElse("номер-машины-не-найден"); //null

        connectToInternationalCarDB(); //5

        System.out.println(carModel.length()); // NPE
    }


    /**
     * Этот метод делает много тяжелой и долгой работы!
     */
    public static void connectToInternationalCarDB() {
        try {
            Thread.sleep(50000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
