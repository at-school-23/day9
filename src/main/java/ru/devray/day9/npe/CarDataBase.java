package ru.devray.day9.npe;

import java.util.Map;
import java.util.Optional;

public class CarDataBase {

    public static Map<String, String> regionalAutoDataBase = Map.of(
        "y234ав777", "Kia Rio",
        "м212ак799", "Lada Largus",
        "в344мк199", "Renault Duster",
        "т732мн777", "Hyundai Porter",
        "а103ну999", "ВАЗ 2104",
        "н455ку199", "Opel Astra"
    );

    //fail-fast
    public static String getCarByRegisterNumber(String registerNumber) {
        System.out.println("Ищу машину в региональной базе...");
        String carModel = regionalAutoDataBase.get(registerNumber);
        if (carModel != null) {
            return carModel;
        }
        throw new RuntimeException("Автомобиль не был найден!");
    }

    //fail-fast
    public static Optional<String> getCarByRegisterNumber2(String registerNumber) {
        System.out.println("Ищу машину в региональной базе...");
        Optional<String> searchResult = Optional.ofNullable(regionalAutoDataBase.get(registerNumber));
        return searchResult;
//        throw new RuntimeException("Автомобиль не был найден!");
    }
}
