package ru.devray.day9;

import java.util.List;
import java.util.Optional;

public class StreamStringsToNumbers {
    public static void main(String[] args) {
//        List<String> list = List.of("103", "-27", "40", "11", "-7", "0");
        List<String> list = List.of("103", "40", "11", "0");

        //Integer vs null
        Optional<Integer> first = list.stream()
//                .map(s -> Integer.valueOf(s))
                .map(Integer::valueOf)
                .peek(System.out::println)
                .filter(i -> i < 0)
                .findFirst();
//        first.orElse(999);
//        first.orElseThrow()
//        first.orElseGet(System::currentTimeMillis);

        System.out.println(first);

        if (first.isPresent()) {
            Integer result = first.get();
        } else {
            Integer result = 999;
        }

//        System.out.println(i1);
    }
}
