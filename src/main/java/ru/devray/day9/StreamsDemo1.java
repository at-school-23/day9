package ru.devray.day9;

import ru.devray.day9.dto.Person;
import ru.devray.day9.dto.Student;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamsDemo1 {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.add(new Student("Говард", "Рорк", 25, 4));
        students.add(new Student("Гарри", "Поттер", 17, 2));
        students.add(new Student("Хари", "Селдон", 43, 5));
        students.add(new Student("Константин", "Циолковский", 34, 2));
        students.add(new Student("Эраст", "Фандорин", 34, 2));
        students.add(new Student("Илон", "Маск", 28, 1));
        students.add(new Student("Дэвид", "Мартинез", 28, 1));
        students.add(new Student("Джон", "Шепард", 31, 2));

//        for (Student student : students) {
//            System.out.println(student);
//        }

        students.stream().forEach(student -> System.out.println(student));

//        System.out.println("---");
//        students.sort((o1, o2) -> o1.age - o2.age);
        students.stream()
                .peek(s -> System.out.println(s))
                .sorted((o1, o2) -> o1.age - o2.age)
                .filter(s -> s.age > 25)
                .forEach(s -> System.out.println(s));

//
//        for (Student student : students) {
//            System.out.println(student);
//        }
//
//        List<Student> students2 = new ArrayList<>();
//        for (Student s : students) {
//            if (s.age > 25) {
//                students2.add(s);
//            }
//        }
//
//        System.out.println("===");
//        for (Student student : students2) {
//            System.out.println(student);
//        }


    }
}
